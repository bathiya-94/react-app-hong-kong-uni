import React, {Component} from "react";
import {Button, Col,  Label, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import {Control, LocalForm, Errors} from "react-redux-form";



const  required  = (val) => val && val.length;
const  maxLength = (len) => (val) => !(val) || (val.length <= len);
const  minLength = (len) => (val) => (val) && (val.length >= len);


class CommentForm extends  Component{
    constructor(props) {
        super(props);

        console.log(this.props.dishId)
        this.state = {
            isModalOpen : false
        }

        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleModal(){
        this.setState({
            isModalOpen : !this.state.isModalOpen
        });
    }

    handleSubmit(values){
        this.toggleModal()
        //alert("Current State is:" +JSON.stringify(values));
        //console.log(values.rating);
        this.props.postComment(this.props.dishId, values.rating, values.author,values.comment);

    }

    renderModal () {
        return(
            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader  toggle={this.toggleModal}>Submit Comment</ModalHeader>
                <ModalBody>
                    <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                        <Row className="form-group">
                            <Label htmlFor="rating" md={2}>Rating</Label>
                            <Col md={10}>
                                <Control.select model=".rating"  id="rating" name="rating"
                                       className= "form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="author" md={2}>Your Name</Label>
                            <Col md={10}>
                                <Control.text model=".author"  id="password" name="password"
                                              placeholder="Your name"
                                              className="form-control"
                                                validators={{required, maxLength: maxLength(15),
                                               minLength: minLength(3) }}/>
                                <Errors model=".author"
                                        className="text-danger"
                                        show = "touched"
                                        messages={{
                                            required : 'Required',
                                            minLength: 'Must be greater than 2 numbers',
                                            maxLength : 'Must be less than 15 numbers'
                                        }}
                                  />
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="comment" md={2}>Comment</Label>
                            <Col md={10}>
                                <Control.textarea model=".comment" name="comment"
                                                  id="comment"
                                      className="form-control "/>
                            </Col>
                        </Row>
                        <Button type="submit" value="submit" color="primary">
                           Submit
                        </Button>
                    </LocalForm>
                </ModalBody>
            </Modal>
        );
    }
    render() {
      return (
          <div>
              <Button color="outline-secondary"  onClick={this.toggleModal}>
                  <span className="fa fa-pencil"> Submit Comment</span>
              </Button>
              {this.renderModal()}
          </div>
      );
    }
}
export  default  CommentForm;