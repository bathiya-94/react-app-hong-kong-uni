import React from "react";
import {Fade, Media} from 'reactstrap';
import {baseUrl} from "../shared/baseUrl";

const Leader = (props) =>{

    if (props !=null){
        return (
            <Fade in>
                <div className="row">
                    <Media>
                        <Media left href="#">
                            {/*<Media image-src={props.leader.image} alt="Generic placeholder image" />*/}
                            <img className="mr-3" src={baseUrl + props.leader.image} alt={props.leader.name}/>
                        </Media>
                        <Media body>
                            <Media heading>
                                {props.leader.name}
                            </Media>
                            <h6>{props.leader.designation}</h6>
                            <p>{props.leader.description}</p>
                        </Media>
                    </Media>

                </div>
            </Fade>
        );
    }

}

export   default  Leader;