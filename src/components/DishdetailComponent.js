import React from "react";
import {
    Card, CardImg, CardText, CardBody, CardTitle,
    Breadcrumb, BreadcrumbItem} from "reactstrap";
import {Link} from "react-router-dom";
import  CommentForm from  "./CommentFormComponent";
import {Loading} from "./LoadingComponent";
import  {baseUrl} from "../shared/baseUrl";
import  {FadeTransform, Fade ,Stagger} from 'react-animation-components';

function renderComments(comments) {
    if (comments != null) {
        const commentList = comments.map((comment) => {
            return (
                <Stagger in>
                    <ul className="list-unstyled" key={comment.id}>
                        <Fade in>
                            <li>{comment.comment}</li>
                            <li>-- {comment.author} , {new Intl.DateTimeFormat('en-US', {
                                year: 'numeric',
                                month: 'short',
                                day: '2-digit'
                            }).format(new Date(Date.parse(comment.date)))}</li>
                        </Fade>
                    </ul>
                </Stagger>
            );
        });

        return (
            <div>
                <h4>Comments</h4>
                <div>{commentList}</div>
            </div>
        );
    } else {
        return (
            <div/>
        );
    }
}

const Dishdetail = (props) => {
    if (props.isLoading){
        return (
          <div className="container">
              <div className="row">
                  <Loading/>
              </div>
          </div>
        );
    }
    else if (props.errMess){
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (props.dish != null)
        {
            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            {/*<BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>*/}
                            <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.dish.name} </BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.dish.name}</h3>
                            <hr/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-5 m-1">
                            <FadeTransform in
                                           transformProps={{
                                               exitTransform: 'scale(0.5) translateY(-50%)'
                                           }}>
                                <Card>
                                    <CardImg width="100" src = {baseUrl +props.dish.image} alt={props.dish.name}/>
                                    <CardBody>
                                        <CardTitle>{props.dish.name}</CardTitle>
                                        <CardText>{props.dish.description}</CardText>
                                    </CardBody>
                                </Card>
                            </FadeTransform>
                        </div>
                        <div   className="col-12 col-md-5 m-1">
                            {renderComments(props.comments)}
                            <CommentForm dishId ={props.dish.id}
                               postComment = {props.postComment} />
                        </div>
                    </div>
                </div>
            );
        }
    else {
        return (<div/>
        );
    }
}



export  default  Dishdetail;